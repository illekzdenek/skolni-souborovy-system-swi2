'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
const pg = require('pg')
const config = require('../config/config.json')[env];
const db = {};

let sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], {
    username: "x",
    password: "x",
    database: "x",
    host: "127.0.0.1",
    port: 5432,
    dialect: "postgres",
    dialectModule: pg
  });
} else {
  //sequelize = new Sequelize(config.database, config.username, config.password, config);
  sequelize = new Sequelize({
    username: "x",
    password: "x",
    database: "x",
    host: "127.0.0.1",
    port: 5432,
    dialect: "postgres",
    dialectModule: pg
  });
}
console.log(sequelize)

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes);
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
