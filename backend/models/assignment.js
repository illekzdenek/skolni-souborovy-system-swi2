'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Assignment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({Teacher, Task}) {
      // define association here
      this.belongsTo(Teacher, {foreignKey: "teacher_id"}) 
      this.hasOne(Task, {foreignKey: "assignment_id"})
    }
    /*toJSON(){
      return { ...this.get(), assignment_id: undefined}//přepíše defaultní funkci toJSON() tak že nevrací id (bezpečnost I guess)
    }*/
  };
  Assignment.init({
    assignment_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },  
    teacher_id:{
      type: DataTypes.INTEGER,
      allowNull: false
    },
    file_path: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {msg: "Soubor není zadán"}, //je vhodné tyhle validace implementovat i na klientovi kvuli UX
        notEmpty: {msg: "Soubor není zadán"} //validace před vložením do DB
      }
    },
    date_created: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'assignment',
    modelName: 'Assignment',
    timestamps: false
  });
  return Assignment;
};
