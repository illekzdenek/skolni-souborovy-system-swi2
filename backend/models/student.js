'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Student extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({Class, Task_solution}) {
      // define association here
      this.belongsTo(Class, {foreignKey: "class_id"})
      this.hasMany(Task_solution, {foreignKey: "student_id"})
    }
    /*toJSON(){
      return { ...this.get(), student_id: undefined} //přepíše defaultní funkci toJSON() tak že nevrací id (bezpečnost I guess)
    }*/
  };
  Student.init({
    student_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },  
    class_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    }, 
    birth_date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
    surname:{
      type: DataTypes.STRING(50),
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: true,
    }, 
    email_parent: {
      type: DataTypes.STRING(50),
      allowNull: false,
    }, 
    phone_parent: {
      type: DataTypes.STRING(50),
      allowNull: false,
    }, 
    login: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING(50),
      allowNull: false,
    }
  }, {
    sequelize,
    tableName: 'student',
    modelName: 'Student',
    timestamps: false
  });
  return Student;
};
