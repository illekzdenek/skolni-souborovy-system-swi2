'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Teacher_to_subject extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({Teacher, Subject}) {
      // define association here
      this.belongsTo(Teacher, {foreignKey: "teacher_id"}) 
      this.belongsTo(Subject, {foreignKey: "subject_id"})
    }
  };
  Teacher_to_subject.init({
    teacher_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false
    },  
    subject_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false
    } 
  }, {
    sequelize,
    tableName: 'teacher_to_subject',
    modelName: 'Teacher_to_subject',
    timestamps: false
  });
  return Teacher_to_subject;
};
