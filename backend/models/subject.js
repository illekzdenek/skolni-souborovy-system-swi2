'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Subject extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({Class_to_subject, Teacher_to_subject, Task}) {
      // define association here
      this.hasMany(Class_to_subject, {foreignKey: "subject_id"})
      this.hasMany(Teacher_to_subject, {foreignKey: "subject_id"})
      this.hasMany(Task, {foreignKey: "subject_id"})
    }
    /*toJSON(){
      return { ...this.get(), subject_id: undefined}//přepíše defaultní funkci toJSON() tak že nevrací id (bezpečnost I guess)
    }*/
  };
  Subject.init({
    subject_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },  
    name: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    capacity: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'subject',
    modelName: 'Subject',
    timestamps: false
  });
  return Subject;
};
