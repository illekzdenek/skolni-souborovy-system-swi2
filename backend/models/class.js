'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Class extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({Teacher, Class_to_subject}) {
      // define association here
      this.belongsTo(Teacher, {foreignKey: "teacher_id"})
      this.hasMany(Class_to_subject, {foreignKey: "class_id"})
    }
    /*toJSON(){
      return { ...this.get(), class_id: undefined}//přepíše defaultní funkci toJSON() tak že nevrací id (bezpečnost I guess)
    }*/
  };
  Class.init({
    class_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },  
    teacher_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    year: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    capacity: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'class',
    modelName: 'Class',
    timestamps: false
  });
  return Class;
};
