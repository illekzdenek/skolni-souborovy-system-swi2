'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Task_solution extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({Task, Student}) {
      // define association here
      this.belongsTo(Student, {foreignKey: "student_id"}) 
      this.belongsTo(Task, {foreignKey: "task_id"})
    }
    /*toJSON(){
      return { ...this.get(), task_solution_id: undefined}//přepíše defaultní funkci toJSON() tak že nevrací id (bezpečnost I guess)
    }*/
  };
  Task_solution.init({
    task_solution_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },  
    task_id:{
      type: DataTypes.INTEGER,
      allowNull: false
    },
    student_id:{
      type: DataTypes.INTEGER,
      allowNull: false
    },
    file_path: {
      type: DataTypes.STRING(100),
      allowNull: false,
      validate: {
        notNull: {msg: "Soubor není zadán"}, //je vhodné tyhle validace implementovat i na klientovi kvuli UX
        notEmpty: {msg: "Soubor není zadán"} //validace před vložením do DB
      }
    },
    score: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    date_submitted: {
      type: DataTypes.DATE,
      allowNull: true // reseno v databazi
    }
  }, {
    sequelize,
    tableName: 'task_solution',
    modelName: 'Task_solution',
    timestamps: false
  });
  return Task_solution;
};
