'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Teacher extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({Assignment, Class, Teacher_to_subject}) {
      // define association here
      this.hasMany(Assignment, {foreignKey: "teacher_id"}) 
      this.hasOne(Class, {foreignKey: "teacher_id"})
      this.hasMany(Teacher_to_subject, {foreignKey: "teacher_id"}) 
    }
    /*toJSON(){
      return { ...this.get(), teacher_id: undefined} //přepíše defaultní funkci toJSON() tak že nevrací id (bezpečnost I guess)
    }*/
  };
  Teacher.init({
    teacher_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },  
    name: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    surname:{
      type: DataTypes.STRING(50),
      allowNull: false
    },
    title: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: true
    }, 
    login: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    password: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'teacher',
    modelName: 'Teacher',
    timestamps: false
  });
  return Teacher;
};
