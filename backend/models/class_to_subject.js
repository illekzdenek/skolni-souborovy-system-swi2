'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Class_to_subject extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({Class, Subject}) {
      // define association here
      this.belongsTo(Class, {foreignKey: "class_id"}) 
      this.belongsTo(Subject, {foreignKey: "subject_id"})
    }
  };
  Class_to_subject.init({
    class_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },  
    subject_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    }  
  }, {
    sequelize,
    tableName: 'class_to_subject',
    modelName: 'Class_to_subject',
    timestamps: false
  });
  return Class_to_subject;
};
