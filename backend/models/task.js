'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Task extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Assignment, Subject, Task_solution}) {
      // define association here
      this.belongsTo(Assignment, {foreignKey: "assignment_id"})
      this.belongsTo(Subject, {foreignKey: "subject_id"})
      this.hasMany(Task_solution, {foreignKey: "task_id"})
    }
    /*toJSON(){
      return { ...this.get(), task_id: undefined}//přepíše defaultní funkci toJSON() tak že nevrací id (bezpečnost I guess)
    }*/
  };
  Task.init({
    task_id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },  
    assignment_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    subject_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    description: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    max_score: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    mandatory: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    date_created: {
      type: DataTypes.DATE
    },
    deadline: {
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    tableName: 'task',
    modelName: 'Task',
    timestamps: false
  });
  return Task;
};
