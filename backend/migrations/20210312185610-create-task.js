'use strict';

module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.createTable('task', {
      task_id: {
        type: DataTypes.INTEGER,
        primaryKey: true
      },  
      assignment_id: {
        type: DataTypes.INTEGER,
        allowNull: true
      },
      subject_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      name: {
        type: DataTypes.STRING(100),
        allowNull: false
      },
      description: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      max_score: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
      mandatory: {
        type: DataTypes.BOOLEAN,
        allowNull: false
      },
      date_created: {
        type: DataTypes.DATE
      },
      deadline: {
        type: DataTypes.DATE
      }
    });
  },
  down: async (queryInterface, DataTypes) => {
    await queryInterface.dropTable('task');
  }
};
