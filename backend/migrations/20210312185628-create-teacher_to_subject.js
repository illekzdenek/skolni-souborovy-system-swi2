'use strict';

module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.createTable('teacher_to_subject', {
      teacher_id: {
        type: DataTypes.INTEGER,
        primaryKey: true
      },  
      subject_id: {
        type: DataTypes.INTEGER,
        primaryKey: true
      }
    });
  },
  down: async (queryInterface, DataTypes) => {
    await queryInterface.dropTable('teacher_to_subject');
  }
};
