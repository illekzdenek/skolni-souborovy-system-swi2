'use strict';

module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.createTable('task_solution', {
      task_solution_id: {
        type: DataTypes.INTEGER,
        primaryKey: true
      },  
      task_id:{
        type: DataTypes.INTEGER,
        allowNull: false
      },
      student_id:{
        type: DataTypes.INTEGER,
        allowNull: false
      },
      file_path: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      score: {
        type: DataTypes.INTEGER,
        allowNull: true
      },
      date_submitted: {
        type: DataTypes.DATE,
        allowNull: false
      }
    });
  },
  down: async (queryInterface, DataTypes) => {
    await queryInterface.dropTable('task_solution');
  }
};
