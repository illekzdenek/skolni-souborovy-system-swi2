'use strict';

module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.createTable('student', {
      student_id: {
        type: DataTypes.INTEGER,
        primaryKey: true
      },  
      class_id: {
        type: DataTypes.INTEGER,
        allowNull: false
      }, 
      birth_date: {
        type: DataTypes.DATEONLY,
        allowNull: true
      },
      name: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      surname:{
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING(50),
        allowNull: true,
      }, 
      email_parent: {
        type: DataTypes.STRING(50),
        allowNull: false,
      }, 
      phone_parent: {
        type: DataTypes.STRING(50),
        allowNull: false,
      }, 
      login: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      password: {
        type: DataTypes.STRING(50),
        allowNull: false,
      }
    });
  },
  down: async (queryInterface, DataTypes) => {
    await queryInterface.dropTable('student');
  }
};
