'use strict';

module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.createTable('assignment', {
      assignment_id: {
        type: DataTypes.INTEGER,
        primaryKey: true
      },  
      teacher_id:{
        type: DataTypes.INTEGER,
        allowNull: false
      },
      file_path: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      date_created: {
        type: DataTypes.DATE,
        allowNull: false
      }
    });
  },
  down: async (queryInterface, DataTypes) => {
    await queryInterface.dropTable('assignment');
  }
};
