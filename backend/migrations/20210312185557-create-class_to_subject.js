'use strict';

module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.createTable('class_to_subject', {
      class_id: {
        type: DataTypes.INTEGER,
        primaryKey: true
      },  
      subject_id: {
        type: DataTypes.INTEGER,
        primaryKey: true
      }
    });
  },
  down: async (queryInterface, DataTypes) => {
    await queryInterface.dropTable('class_to_subject');
  }
};
