'use strict';

module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.createTable('teacher', {
      teacher_id: {
        type: DataTypes.INTEGER,
        primaryKey: true
      },  
      name: {
        type: DataTypes.STRING(50),
        allowNull: false
      },
      surname:{
        type: DataTypes.STRING(50),
        allowNull: false
      },
      title: {
        type: DataTypes.STRING(50),
        allowNull: true
      },
      email: {
        type: DataTypes.STRING(50),
        allowNull: true
      }, 
      login: {
        type: DataTypes.STRING(50),
        allowNull: false
      },
      password: {
        type: DataTypes.STRING(50),
        allowNull: false
      }
    });
  },
  down: async (queryInterface, DataTypes) => {
    await queryInterface.dropTable('teacher');
  }
};
