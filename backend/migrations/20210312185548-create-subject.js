'use strict';

module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.createTable('subject', {
      subject_id: {
        type: DataTypes.INTEGER,
        primaryKey: true
      },  
      name: {
        type: DataTypes.STRING(50),
        allowNull: false
      },
      capacity: {
        type: DataTypes.STRING(50),
        allowNull: false
      }
    });
  },
  down: async (queryInterface, DataTypes) => {
    await queryInterface.dropTable('subject');
  }
};
