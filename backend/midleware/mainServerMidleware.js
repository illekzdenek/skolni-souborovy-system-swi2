const {sequelize, User} = require('../models');
const jwt = require("jsonwebtoken");
const authHelper = require("../helpers/authHelper")

async function getFile(req,res,next){ // midleware funkce
    let file;
    try {
        file = await File.findById(req.params.id)
        if(file === null){
            return res.status(404).json({message: "Cannot find File"})
        }
    } catch (error) {
        return res.status(500).json({message: error.message})
    }

    res.file = file
    next()
}
//kontroluje přihlášení uživatele
function authenticateToken(req,res,next){
    const authHeader = req.headers["authorization"] //vytáhne z hlavičky požadavku autorizační položku
    const token = authHeader && authHeader.split(" ")[1] // jestli existuje hlavička a její 2. část

    if(token === null){
        return res.sendStatus(401)
    }
    jwt.verify(token, ""+process.env.ACCESS_TOKEN_SECRET, (error, tokenData) => {
        if(error) return res.sendStatus(403)
        req.tokenData = tokenData
        next()
    })
}
async function getUserByUuid(req,res,next){ // midleware funkce
    let user;
    try {
        const uuid = req.body.uuid
        console.log(uuid)
        user = await User.findOne({
            where: {uuid}
        })
        
        console.log(user)
    } catch (error) {
        return res.status(500).json({message: error.message})
    }
    res.user = user
    next()
}


module.exports = {
    getFile,
    getUserByUuid,
    authenticateToken
}