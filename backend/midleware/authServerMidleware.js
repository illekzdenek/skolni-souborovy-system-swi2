const {sequelize, User, Student, Teacher} = require('../models');

const authConstants = require("../constants/authConstants")

async function getUserByMail(req, res, next) { // midleware funkce
    let user = null;
    try {
        console.log(authConstants)
        const email = req.body.email;
        const role = req.body.role;
        if (role === authConstants.ROLE_STUDENT){
            user = await Student.findOne({
                where: {email}
            })
        }
        if (role === authConstants.ROLE_TEACHER){
            user = await Teacher.findOne({
                where: {email}
            })
        }

        console.log(user);
        
    } catch (error) {
        console.log(':(')
        return res.status(500).json({message: error.message})
    }
    res.user = user;
    next()
}

async function getUserByLogin(req, res, next) { // midleware funkce
    let user = null;
    let role = "";
    try {
        console.log(authConstants)
        console.log(Student)
        console.log(Teacher)
        const login = req.body.login;
        const password = req.body.password;
        user = await Student.findOne({
            where: {login}
        })
        if (user !== null){
            role = authConstants.ROLE_STUDENT
        }
        if (user === null){
            user = await Teacher.findOne({
                where: {login}
            })
            role = authConstants.ROLE_TEACHER
        }

        console.log(user);
        
    } catch (error) {
        console.log(error.message)
        return res.status(500).json({message: error.message})
    }
    res.user = user;
    res.role = role;
    next()
}

async function getUserByUuid(req, res, next) { // midleware funkce
    let user;
    try {
        const uuid = req.params.uuid;
        console.log(uuid);
        user = await User.findOne({
            where: {uuid}
        })
    } catch (error) {
        return res.status(500).json({message: error.message})
    }
    res.user = user;
    next()
}

module.exports = {
    getUserByMail,
    getUserByUuid,
    getUserByLogin
}