const toExport = {
    ROLE_TEACHER : "teacher",
    ROLE_STUDENT : "student"
}

module.exports = toExport