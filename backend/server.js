require("dotenv").config() //nastaví .env soubor =>> používání systémových proměnných > užívat pro ochranu citlivých údajů
const express = require('express') 
const app = express()
const cors = require('cors')

app.use(express.json({limit: '10mb', extended: true}))
app.use(cors())

const fileRouter = require("./routes/fileRoutes")   //vytvoření nové směrovače pro konkrétní cesty
const dataRouter = require("./routes/dataRoutes")
app.use("/files", fileRouter)    //přiřazení url podcesty k směrovači -> v contentRoutes.js jsou pak všechny volání pro "/contents"
app.use("/data", dataRouter)

if (process.env.NODE_ENV === "production"){
    console.log("Production")
    app.use(express.static(__dirname + "/public/"))
    app.get(/.*/, (req, res) => res.sendFile(__dirname + "/public/index.html"))
}

const port = process.env.PORT || 30003
let server = app.listen(port, "localhost", () => {
    var host = server.address().address;
    var port = server.address().port;
    console.log("Server ready and listening at http://%s:%s", host, port)
})    // pokud se uspěšně spustí server

//TODO: https://expressjs.com/en/5x/api.html#app.listen