const fileHelpers = require('../helpers/fileHelper');
const path = require('path');
const testfile_path = path.join('test', 'testfile');
const fs = require('fs');
const test_folder = 'test';
const doc_folder = 'documents';


describe("file helpers function", () => {
    test("getFile function", () => {

        expect(typeof fileHelpers.getFile(testfile_path)).toEqual('object')
    });

    test("save file functions", () => {
        file = {
            name: "testfile",
            extension: 'txt',
            data: "data:thisIsTestFile;base64,lala"
        };

        const tfile_path = fileHelpers.saveFile(file, test_folder);
        expect(typeof testfile_path).toEqual('string')
    })

    test("remove file", () => {

        let dir = path.join(path.parse(__dirname).dir, doc_folder, test_folder);
        fs.readdirSync(dir).forEach(
            file => {
                if (file != 'testfile'){
                    fileHelpers.removeFile(path.join(test_folder,file));
                    expect(fs.existsSync(file)).toBeFalsy()
                }

            }
        );

    })
});

