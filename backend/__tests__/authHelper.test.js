const authHelper = require('../helpers/authHelper')

describe("auth helpers function", () => {
        test('decode token', () => {
            var test_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'
            expect(authHelper.decodeToken(test_token)).toEqual({
                sub: '1234567890', name: 'John Doe', iat: 1516239022
            })
        })

        test('generate token', () => {
            var user = {
                name: "John",
                last_name: "Doe"
            }
            expect(authHelper.generateAccessToken(user)).toBeDefined()
        })

    }
)
