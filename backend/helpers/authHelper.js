const jwt = require("jsonwebtoken");
const atob = require("atob")
function generateAccessToken(user) {
    return jwt.sign(user, ""+process.env.ACCESS_TOKEN_SECRET, {expiresIn: "1m"})
}

function decodeToken (token){
    return JSON.parse(atob(token.split('.')[1]))
}

module.exports = {
    generateAccessToken,
    decodeToken
}