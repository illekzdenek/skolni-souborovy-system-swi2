
const path = require('path');
const fs = require('fs');

const document_dir = './documents';

function saveFile(file, folder) {
    // function save file_content in documents dir with name generated from timestamp
    file_content = base64ToString(file.data) //base64 transformed to data 

    try {
        file_name = new Date().getTime().toString();
        const fileDir =  path.join(document_dir, folder,)
        file_path = path.join(fileDir, file.name+file_name+"."+file.extension);
        if (!(fs.existsSync(fileDir))) {
            fs.mkdirSync(fileDir)
        }
        if (!(fs.existsSync(file_path))){
            fs.writeFile(file_path, file_content.data, (err) => { 
                if (err) { 
                    console.log(err); 
                }
            });
            return file_path
        }
    } catch (e) {
        console.log('File couldn\'t be saved ');
        console.error(e)
    }

}

function getFile(file_path) {
    // function retrieving file from app
    try {

        return fs.readFileSync(path.join(document_dir, file_path))
    } catch (e) {
        console.log('File couldn\'t be loaded');
        console.error(e)

    }
}

function removeFile(file_path) {
    try {
        
        fs.unlinkSync(path.join("", file_path))
    } catch (e) {
        console.log('File couldn\'t be removed');
        console.error(e)
    }
}

function base64ToString(base64) {
    //ořezání base64 řetězce
    console.log(base64.split(",")[0])
    //var matches = base64.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/), přísnější (některé sooubory (docx) nefungovali)
    response = {};
    const matches = base64.split(",")
    
    if (matches.length !== 2) {
        return new Error('Invalid input string');
    }

    response.type = matches[0];
    response.data = new Buffer.from(matches[1], 'base64');

    return response;

}

module.exports = {
    saveFile,
    getFile,
    base64ToString,
    removeFile
}