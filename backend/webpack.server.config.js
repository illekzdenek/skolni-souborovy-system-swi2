const path = require('path')
const webpack = require('webpack')
const nodeExternals = require('webpack-node-externals')
const CopyPlugin = require("copy-webpack-plugin");
const json = require('./config/config.json');
module.exports = {
    entry: {
        server: './server.js',
    },
    output: {
        path: path.join(__dirname, 'dist'),
        publicPath: '/',
        filename: '[name].js'
    },
    target: 'node',
    node: {
        // Need this when working with express, otherwise the build fails
        __dirname: false,   // if you don't put this is, __dirname
        __filename: false,  // and __filename return blank or /
    },
    externals: [
        nodeExternals(),
        'pg',
        'pg-hstore',
        'sqlite3',
        'mysql2'
    ],
    module: {
        noParse : [/pg\/lib\/native/],
        rules: [
            {
                // Transpiles ES6-8 into ES5
                test: /\.js$/,
                exclude: /node_modules/,
                //use: {
                //loader: "babel-loader"
                //}
            },
            //{
            //    test: /\.js$/,
            //    loader: 'string-replace-loader',
            //    options: {
            //        search: '/../config/config.json',
            //        replace: "\\\\config.json"
            //    }
            //}
        ]
    },
    //resolve: {
    //    alias:{
    //        config: path.resolve(__dirname, '/config')
    //    }
    //},
    plugins: [
        new webpack.IgnorePlugin({ resourceRegExp: /^pg-native$/ })
    //    new CopyPlugin({
    //    patterns: [
    //        { from: "./config/config.json", to: "./config/config.json" }
    //    ],
    //    }),
    ],
}