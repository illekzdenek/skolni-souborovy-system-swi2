const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken")
const {Assignment, Task, Task_solution } = require('../models')
require("dotenv").config()

const fileHelper = require("../helpers/fileHelper");
const midleware = require('../midleware/mainServerMidleware');
const authConstants = require("../constants/authConstants");
//Get server is online
router.get("/test", async (req, res) => {
    console.log(Task)
    try {
        res.json({
            message: "MainServer available",
            online: true
        })
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})

//Get all files
router.get("/files", midleware.authenticateToken, async (req, res) => {
    try {
        const files = await File.findAll({include:['user']}); //dá se říct že je to něco jako JOIN, k záznamu z files tabulky přidá odpovídající záznam z user tabulky jako objekt user | je 'user' je alias definovaný ve files.js v asociacích
        return res.json(files)
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})
//Create one 
router.post("/files", midleware.getUserByUuid, async (req, res) => {
    try {
        const {name, type, size} = req.body
        console.log(name,type,size,res.user.id)
        const file = await File.create({name, type, size, userId: res.user.id})
        res.status(201).json(file)
    } catch (error) {
        res.status(400).json(error)
    }
})

//Create task solution
router.post("/create_task_solution", midleware.authenticateToken, async (req, res) => {
    try {
        const folder = "\\taskSolutions"
        if (req.tokenData.role === authConstants.ROLE_STUDENT){
            const student_id        = req.tokenData.user.student_id
            const {file, task_id}   = req.body
            const task              = await Task.findOne({where: {task_id: task_id}})
            // Kontrola překročení deadlinu
            if (Date.now() > Date.parse(task.deadline)) {
                res.status(500).json({message: 'odevzdárna je již uzavřena'})
            } else {
                const file_path     = fileHelper.saveFile(file,folder)
                const task_solution = await Task_solution.create({student_id, task_id, file_path})
                res.status(201).json(task_solution)
            }
        } else {
            res.status(403).json({message: 'neoprávněný přístup'})
        }
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})

// Create assignment
router.post("/create_assignment", midleware.authenticateToken, async (req, res) => {
    const tokenData = req.tokenData;
    if (tokenData.role === authConstants.ROLE_TEACHER) {
        try {
            const folder = "\\assignments"
            const { file } = req.body
            const teacher_id = tokenData.user.teacher_id;
            const file_path = fileHelper.saveFile(file,folder);
            const assignment = await Assignment.create({teacher_id, file_path});
            res.status(201).json(assignment)
        } catch (error) {
            res.status(500).json({message: error.message})
        }
    } else {
        res.status(500).json({message: "You are not a teacher"})
    }
})

// Edit assignment teacher_id or file_path, returns newly edited task
router.post("/edit_task", midleware.authenticateToken, async (req, res) => {
    const tokenData = req.tokenData;
    if (tokenData.role === authConstants.ROLE_TEACHER) {
        try {
            const { file, assignment_id, teacher_id, task_id, name, description, max_score, mandatory, deadline, deletedFile } = req.body
            const old_teacher_id = tokenData.user.teacher_id;
            
            // TASK
            const oldTask = await Task.findOne({where: { task_id }});
            await Task.update({
                name: name || oldTask.name,
                description: description || oldTask.description,
                max_score: max_score || oldTask.max_score,
                mandatory: mandatory,
                deadline: deadline || oldTask. deadline
            }, {
                where: {
                    task_id: task_id
                }
            });

            // ASSIGNMENT
            if (file){
                let folder = "assignments";
                file_path = "";
                try{
                    // task ma existujici soubor
                    const oldAssignment = await Assignment.findOne({where: { assignment_id }});
                    let file_path = oldAssignment.file_path;
                    fileHelper.removeFile(oldAssignment.file_path); // old file removed
                    file_path = fileHelper.saveFile(file, folder); // new file saved
                    await Assignment.update({
                        file_path: file_path, // update file_path if changed
                        teacher_id: teacher_id || old_teacher_id // update teacher_id if changed
                    }, {
                        where: {
                            assignment_id: assignment_id
                        }
                    });
                } catch(error){
                    // task nema existujici soubor
                    file_path = fileHelper.saveFile(file, folder); // new file saved
                    const newAssignment = await Assignment.create({teacher_id:old_teacher_id, file_path});
                    const oldTaskWithoutFile = await Task.findOne({where: { task_id }});
                    await Task.update({
                        assignment_id: newAssignment.assignment_id || oldTaskWithoutFile.assignment_id,
                    }, {
                        where: {
                            task_id: task_id
                        }
                    });
                }
            } else if(!file && deletedFile){
                // ucitel kliknul odstranit soubor a nepridal novej soubor
                const oldTaskWithFile = await Task.findOne({where: { task_id }});
                await Task.update({
                    assignment_id: null,
                }, {
                    where: {
                        task_id: task_id
                    }
                });
                const oldAssignment = await Assignment.findOne({where: { assignment_id }});
                fileHelper.removeFile(oldAssignment.file_path); // old file removed
                await oldAssignment.destroy() // smaz soubor z databaze
            }

            const updatedTask = await Task.findOne({where: { task_id }});
            res.json(updatedTask)
        } catch (error) {
            res.status(500).json({message: error.message})
        }
    } else {
        res.status(500).json({message: "You are not a teacher"})
    }
})

router.post("/remove_task_solution", midleware.authenticateToken , async (req, res) => {
    const tokenData = req.tokenData;
    console.log(tokenData)
    if (tokenData.role === authConstants.ROLE_TEACHER) {
        try {
            const {task_solution_id} = req.body
            const fileToRemove = await Task_solution.findOne({where: { task_solution_id }});

            // Remove from database
            const taskSolution = await Task_solution.destroy({where: { task_solution_id }});
            // Delete file
            fileHelper.removeFile(fileToRemove.file_path)
            res.json({message: "Task solution was removed"})
        } catch (error) {
            res.status(500).json({message: error.message})
        }
    } else {
        res.status(500).json({message: "You are not a teacher"})
    }
})

router.post("/remove_assignment", midleware.authenticateToken , async (req, res) => {
    const tokenData = req.tokenData;
    console.log(tokenData)
    if (tokenData.role === authConstants.ROLE_TEACHER) {
        try {
            const {assignment_id} = req.body
            const taskSolutionsIds = await Task_solution.findAll({
                include: [{ 
                    model: Task, include: [{ 
                        model: Assignment,
                        where: { assignment_id: assignment_id }
                    }],
                    right: true
                }]
            });
            if (taskSolutionsIds.len != 0) {
                //delete each task_solution by id
                taskSolutionsIds.forEach(obj => {
                    fileHelper.removeFile(obj.file_path)
                    Task_solution.destroy({where: { task_solution_id: obj.task_solution_id }})
                })
            };

            const fileToRemove = await Assignment.findOne({where: { assignment_id }});
            
            // Remove assignment and tasks from database
            const tasks = await Task.destroy({where: { assignment_id }});
            const assignment = await Assignment.destroy({ where: { assignment_id }});
            // Delete file
            fileHelper.removeFile(fileToRemove.file_path)
            res.json({message: "Assignment was removed"})
            
        } catch (error) {
            res.status(500).json({message: error.message})
        }
    } else {
        res.status(500).json({message: "You are not a teacher"})
    }
})

//Get one
router.get("/:id", midleware.getFile ,(req, res) => { //getFile je midleware funkce níže
    res.send(res.file.name)
})


module.exports = router