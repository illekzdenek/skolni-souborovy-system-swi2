const express = require("express");
const router = express.Router();
const {Teacher, Assignment, Class_to_subject, Class, Student, Subject, Task_solution, Task, Teacher_to_subject } = require('../models')
const fileHelper = require("../helpers/fileHelper")
require("dotenv").config()

const mainServerMidleware = require("../midleware/mainServerMidleware");
const authConstants = require("../constants/authConstants");

//Get all teachers
router.get("/teachers", async (req, res) => {
    try {
        const teachers = await Teacher.findAll(); //.findAll() jako parametry se uvedou sloupce, které chceme
        res.json(teachers)
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})

//Get teacher by id
router.get("/teacher/:id", async (req, res) => {
    let teacher;
    try {
        const teacher_id = req.params.id
        console.log(teacher_id)
        teacher = await Teacher.findOne({
            where: {teacher_id}
        })
    } catch (error) {
        return res.status(500).json({message: error.message})
    }
    res.json(teacher)
})

//Get all students
router.get("/students", mainServerMidleware.authenticateToken, async (req, res) => {
    console.log(req.tokenData) //req.tokenData obsahuje data z tokenu, který byl poslán v požadavku, je zprostředkovaný midlewarem
    try {
        const students = await Student.findAll(); //.findAll() jako parametry se uvedou sloupce, které chceme
        res.json(students)
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})

//Get student by id
router.get("/student/:id", mainServerMidleware.authenticateToken, async (req, res) => {
    const tokenData = req.tokenData;
    if (tokenData.role === authConstants.ROLE_TEACHER || tokenData.role === authConstants.ROLE_STUDENT ) {
        try {
            let student;
            const student_id = req.params.id
            student = await Student.findOne({
                where: {student_id}
            })
            
            res.json(student)
        } catch (error) {
            return res.status(500).json({message: error.message})
        }
    } else {
        res.status(500).json({message: "You are not a teacher neither student"})
    }
})

//Get all assignments for teacher
router.get("/assignments", mainServerMidleware.authenticateToken, async (req, res) => {
    const tokenData = req.tokenData;
    if (tokenData.role === authConstants.ROLE_TEACHER) {
        try {
            const teacher_id = tokenData.user.teacher_id;
            const assignments = await Assignment.findAll({
                where: { teacher_id }
            }); 
            res.json(assignments)
        } catch (error) {
            res.status(500).json({message: error.message})
        }
    } else {
        res.status(500).json({message: "You are not a teacher"})
    }
})

//Get file
router.get("/file_download/:folder/:fileName", mainServerMidleware.authenticateToken,  async (req, res) => {
    const tokenData = req.tokenData;
    console.log(tokenData)
    if (tokenData.role === authConstants.ROLE_TEACHER || tokenData.role === authConstants.ROLE_STUDENT ) {
        try {
            const {folder, fileName} = req.params
            console.log(folder, fileName)
            const path = process.cwd()+"\\documents\\"+folder+"\\"+fileName
            
            console.log(path)
            console.log("sending files...")
            res.download(path, "assignment.pdf")
        } catch (error) {
            return res.status(500).json({message: error.message})
        }
    } else {
        res.status(500).json({message: "You are not a teacher neither student"})
    }
})

//Get assignment by id
router.get("/assignment/:id", mainServerMidleware.authenticateToken,  async (req, res) => {
    const tokenData = req.tokenData;
    if (tokenData.role === authConstants.ROLE_TEACHER || tokenData.role === authConstants.ROLE_STUDENT ) {
        let assignment;
        try {
            const assignment_id = req.params.id
            assignment = await Assignment.findOne({
                where: {assignment_id}
            })

            res.json(assignment)
        } catch (error) {
            return res.status(500).json({message: error.message})
        }
    } else {
        res.status(500).json({message: "You are not a teacher neither student"})
    }
})

//Get all classs
router.get("/classes", async (req, res) => {
    try {
        const classs = await Class.findAll(); //.findAll() jako parametry se uvedou sloupce, které chceme
        res.json(classs)
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})

//Get class by id
router.get("/class/:id", async (req, res) => {
    let classVar;
    try {
        const class_id = req.params.id
        classVar = await Class.findOne({
            where: {class_id}
        })
    } catch (error) {
        return res.status(500).json({message: error.message})
    }
    res.json(classVar)
})

//Get all subjects
router.get("/subjects", async (req, res) => {
    try {
        const subjects = await Subject.findAll(); //.findAll() jako parametry se uvedou sloupce, které chceme
        res.json(subjects)
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})

//Get subject by id
router.get("/subject/:id", mainServerMidleware.authenticateToken, async (req, res) => {
    let subject;
    try {
        const subject_id = req.params.id
        subject = await Subject.findOne({
            where: {subject_id}
        })
    } catch (error) {
        return res.status(500).json({message: error.message})
    }
    res.json(subject)
})

//Get all tasks
router.get("/tasks", async (req, res) => {
    try {
        const tasks = await Task.findAll(); //.findAll() jako parametry se uvedou sloupce, které chceme
        res.json(tasks)
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})

//Get task by id
router.get("/task/:id", async (req, res) => {
    let task;
    try {
        const task_id = req.params.id
        task = await Task.findOne({
            where: {task_id}
        })
    } catch (error) {
        return res.status(500).json({message: error.message})
    }
    res.json(task)
})

//Get all tasks by subject
router.get("/subject_tasks/:id",mainServerMidleware.authenticateToken, async (req, res) => {
    try {
        const subject_id = req.params.id;
        const tasks = await Task.findAll({where: { subject_id }}); //.findAll() jako parametry se uvedou sloupce, které chceme
        res.json(tasks)
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})

// Delete task
router.get("/delete_task/:id", mainServerMidleware.authenticateToken, async (req, res) => {
    //res.json({message: "AHOJ"})
    const tokenData = req.tokenData;
    if (tokenData.role === authConstants.ROLE_TEACHER) {
        try {
            const task_id = req.params.id;
            const task = await Task.findOne({where: { task_id: task_id } });
            await task.destroy();
            res.status(200);    
        } catch (error) {
            res.status(500).json({message: error.message})
        }
    } else {
        res.status(500).json({message: "You are not a teacher"})
    }
})

//Get all task_solutions
router.get("/task_solutions", mainServerMidleware.authenticateToken, async (req, res) => {
    const tokenData = req.tokenData;
    if (tokenData.role === authConstants.ROLE_STUDENT) {
        try {
            const student_id = tokenData.user.student_id;
            const task_solutions = await Task_solution.findAll({
                where: { student_id }
            }); 
            res.json(task_solutions)
        } catch (error) {
            res.status(500).json({message: error.message})
        }
    } else if (tokenData.role === authConstants.ROLE_TEACHER) {
        try {
            const teacher_id = tokenData.user.teacher_id;
            const task_solutions = await Task_solution.findAll({
                where: { '$teacher_id$': teacher_id },
                include: [{ 
                    model: Task, include: [{ 
                        model: Assignment
                    }]
                }]
            }); 
            res.json(task_solutions)
        } catch (error) {
            res.status(500).json({message: error.message})
        }
    } else {
        res.status(500).json({message: "You are not a teacher neither student"})
    }
})

//Get solutions for certain task for teacher
router.get("/task_solutions/:task_id", mainServerMidleware.authenticateToken, async (req, res) => {
    const tokenData = req.tokenData;
    if (tokenData.role === authConstants.ROLE_TEACHER) {
        try {
            const teacher_id = tokenData.user.teacher_id;
            const task_id = req.params.task_id;
            const task_solutions = await Task_solution.findAll({
                where: { '$teacher_id$': teacher_id, 'task_id': task_id },
                include: [ 
                    Student, {
                    model: Task, include: [ Subject, Assignment ]
                }]
            });
            res.json(task_solutions)
        } catch (error) {
            res.status(500).json({ message: error.message })
        }
    } else {
        res.status(500).json({ message: "You are not a teacher" })
    }
})


//Get task_solution by id
router.get("/task_solution/:id", mainServerMidleware.authenticateToken, async (req, res) => {
    const tokenData = req.tokenData;
    if (tokenData.role === authConstants.ROLE_TEACHER) {
        try {
            let task_solution = null;
            const task_solution_id = req.params.id
            task_solution = await Task_solution.findOne({
                where: {task_solution_id}
            })
            if (task_solution === null){
                return res.status(404)
            }
            res.json(task_solution)
        } catch (error) {
            return res.status(500).json({message: error.message})
        }
    } else {
        res.status(500).json({message: "You are not a teacher"})
    }   
})

//Get all class_to_subjects
router.get("/class_to_subjects", async (req, res) => {
    try {
        const class_to_subjects = await Class_to_subject.findAll(); //.findAll() jako parametry se uvedou sloupce, které chceme
        res.json(class_to_subjects)
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})

//Get all class_to_subjects by class_id
router.get("/class_to_subjects/:id", mainServerMidleware.authenticateToken, async (req, res) => {
    try {
        console.log(req.params)
        const class_id = req.params.id
        const class_to_subjects = await Class_to_subject.findAll({where:{class_id}}); //.findAll() jako parametry se uvedou sloupce, které chceme
        res.json(class_to_subjects)
    } catch (error) {
        console.log(error)
        res.status(500).json({message: error.message})
    }
})

//Get all teacher_to_subjects
router.get("/teacher_to_subjects", async (req, res) => {
    try {
        const teacher_to_subjects = await Teacher_to_subject.findAll(); //.findAll() jako parametry se uvedou sloupce, které chceme
        res.json(teacher_to_subjects)
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})

//Get all teachers subjects
router.get("/teachers_subjects",mainServerMidleware.authenticateToken, async (req, res) => {
    const tokenData = req.tokenData;
    if (tokenData.role === authConstants.ROLE_TEACHER) {
        try { 
            const teacher_id = tokenData.user.teacher_id;
            const teacherssubjects = await Teacher_to_subject.findAll({where:{teacher_id:teacher_id}, include: [Subject]}); //.findAll() jako parametry se uvedou sloupce, které chceme
            res.json(teacherssubjects)
        } catch (error) {
            res.status(500).json({message: error.message})
        }
    } else {
        res.status(500).json({message: "You are not a teacher"})
    }
})

// Add score to task solution
router.post("/add_score", mainServerMidleware.authenticateToken, async (req, res) => {
    const tokenData = req.tokenData;
    if (tokenData.role === authConstants.ROLE_TEACHER) {
        try {
            const { task_solution_id, task_id, student_id, score } = req.body
            const teacher_id = tokenData.user.teacher_id;
            const task_solution = await Task_solution.update({score: score}, {
                where: {
                    task_solution_id: task_solution_id,
                    student_id: student_id, 
                    task_id: task_id
                }})
            const updated_task_solution = await Task_solution.findOne({ where: {task_solution_id}})
            res.json(updated_task_solution)    
        } catch (error) {
            res.status(500).json({message: error.message})
        }
    } else {
        res.status(500).json({message: "You are not a teacher"})
    }
})

// Create task
router.post("/create_task", mainServerMidleware.authenticateToken, async (req, res) => {
    const tokenData = req.tokenData;
    if (tokenData.role === authConstants.ROLE_TEACHER) {
        try {
            const {assignment_id, subject_id, name, description, max_score, mandatory, deadline } = req.body
            const task = await Task.create({assignment_id, subject_id, name, description, max_score, mandatory, deadline});
            res.json(task)
        } catch (error) {
            res.status(500).json({message: error.message})
        }
    } else {
        res.status(500).json({message: "You are not a teacher"})
    }
})

module.exports = router
