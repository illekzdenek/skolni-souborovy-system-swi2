const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken")
const { sequelize, User, Teacher, Assignment, Class_to_subject, Class, Student, Subject, Task_solution, Task, Teacher_to_subject } = require('../models')
require("dotenv").config()
const authConstants = require("../constants/authConstants")

const midleware = require('../midleware/authServerMidleware')
const fileHelper = require('../helpers/fileHelper')
const authHelper = require('../helpers/authHelper')

let refreshTokens=[]; //tohle do produkce nepatří, v produkci ukládat ideálně do DB

//Get server is online
router.get("/test", async (req, res) => {
    try {
        res.json({
            message: "Authentication available",
            online: true
        })
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})

//Get all users
router.get("/users", async (req, res) => {
    try {
        const users = await User.findAll(); //.findAll() jako parametry se uvedou sloupce, které chceme
        res.json(users)
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})



//Get user by uuid
router.get("/user/:uuid", midleware.getUserByUuid, async (req, res) => {
    res.json(res.user)
})
//Create user (student|teacher)
router.post("/user", midleware.getUserByMail, async (req, res) => {
    if (res.user === null || res.user.email !== req.body.email){
        try {
            console.log(authConstants.ROLE_TEACHER)
            if (req.body.role === authConstants.ROLE_STUDENT){
                const {class_id, birth_date, name, surname, email, email_parent, phone_parent, login, password} = req.body;
                const newUser = await Student.create({
                    class_id, birth_date, name, surname, email, email_parent, phone_parent, login, password
                })
                res.status(201).json(newUser)
            }
            if (req.body.role === authConstants.ROLE_TEACHER){
                const {name, surname, title, email, login, password} = req.body;
                const newUser = await Teacher.create({
                    name, surname, title, email, login, password
                })
                res.status(201).json(newUser)
            }
            
        } catch (error) {
            return res.status(400).json(error.message)
        }
    } else {
        return res.status(409).json({
            message: "Mail already registered."
        })
    }
})
//Remove user
router.delete("/user/:uuid",midleware.getUserByUuid, async (req, res) => {
    if (res.user !== null){
        
        try {
            await res.user.destroy()
            res.json({message: "User was succesfully deleted."})
        } catch (error) {
            res.status(400).json(error.message)
        }
    } else {
        res.status(409).json({
            message: "Mail already registered."
        })
    }
})

//Update user
router.put("/user/:uuid",midleware.getUserByUuid, async (req, res) => {
    if (res.user !== null){
        const {mail, pass} = req.body
        try {
            console.log(res.user)
            res.user.mail = mail
            res.user.pass = pass
            console.log(res.user)
            await res.user.save()
            res.json(res.user)
        } catch (error) {
            res.status(400).json(error.message)
        }
    } else {
        res.status(409).json({
            message: "Mail already registered."
        })
    }
})

//Login user
router.post("/login",midleware.getUserByLogin, async (req, res) => {
    if(res.user === null){
            return res.status(404).json({message: "Cannot find you"})
        }
    if (res.user.password === req.body.password){
        const accessToken = authHelper.generateAccessToken({
            user:res.user.toJSON(),
            role: res.role
        })
        const refreshToken = jwt.sign({
            user:res.user.toJSON(),
            role: res.role
        }, ""+process.env.REFRESH_TOKEN_SECRET)
        refreshTokens.push(refreshToken);
        res.json({
            message: "Login was succesful!",
            logedIn: true,
            accessToken: accessToken,
            refreshToken: refreshToken
        })
    } else {
        res.status(401).json("Wrong mail or password.")
    }
})

//Create refresh TOKEN
router.post("/token", (req,res) => {
    const refreshToken = req.body.token
    if (refreshToken === null){
        res.sendStatus(401)
    }
    if(!refreshTokens.includes(refreshToken)){
        return res.sendStatus(403)
    }
    requestToken = authHelper.decodeToken(req.body.token)
    jwt.verify(refreshToken, ""+process.env.REFRESH_TOKEN_SECRET, (error,user) => {
        if (error) return res.sendStatus(403).json(error.message)
        const accessToken = authHelper.generateAccessToken({
            role:requestToken.role,
            user:requestToken.user
        })
        res.json({accessToken: accessToken})
    })
})

//Delete refresh TOKEN (logout)
router.delete("/logout", (req,res) => {
    refreshTokens = refreshTokens.filter(token=> token !== req.body.token)
    res.sendStatus(204)
})


module.exports = router