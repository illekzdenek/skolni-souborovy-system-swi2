require("dotenv").config() //nastaví .env soubor =>> používání systémových proměnných > užívat pro ochranu citlivých údajů
const express = require('express') 
const app = express()
const cors = require('cors')
const { sequelize } = require('./models')

app.use(express.json())
app.use(cors())

const authServerRouter = require("./routes/authServerRoutes")
app.use("/server", authServerRouter)

if (process.env.NODE_ENV === "production"){
    console.log("Production")
    app.use(express.static(__dirname + "/public/"))
    app.get(/.*/, (req, res) => res.sendFile(__dirname + "/public/index.html"))
}
const port = process.env.PORT || 40004
let server = app.listen(port, "localhost" ,async () => {
    console.log("Authentication server ready!")
    var host = server.address().address;
    var port = server.address().port;
    console.log("Server ready and listening at %s:%s", host, port)
    //await sequelize.authenticate() //force => přepsání schématu tabulky | alter => vytvoří novou tabulku
    console.log("DB connected")
})    // pokud se uspěšně spustí server