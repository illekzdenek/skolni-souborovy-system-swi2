import { createStore } from "vuex";
import * as appView from "../constants/appView";
import * as loginHelper from "../helpers/loginHelper";

export default createStore({
  state: {
    appState:{
      activeView: appView.LOGIN,
      activeViewProps:{},
      mainServerOnline: false,
      authServerOnline: false
    },
    user:{
      debug: false,
      //učitel a společné
      name: "",
      surname: "",
      title: "",
      email: "",
      login: "",
      password: "",
      role: "",
      //student
      class_id: null,
      birth_date: "",
      email_parent: "",
      phone_parent: "",
      //session
      accessToken: "",
      refreshToken: "",
      accessTokenExpiration: null
    },
    task:{
      activeTaskId: 1,
      taskSolution:{
        files: []
      },
      assignment:{
        files:[]
      }
    }
  },
  mutations: {
    setActiveView(state, payload){
      state.appState.activeView = payload
    },
    setActiveViewWithProps(state, payload){
      state.appState.activeView = payload.view
      state.appState.activeViewProps = payload.props
    },
    setMainServerState(state, payload){
      state.appState.mainServerOnline = payload
    },
    setAuthServerState(state, payload){
      state.appState.authServerOnline = payload
    },
    setUserAsStudent(state, payload){
      state.user.class_id = payload.class_id, 
      state.user.birth_date = payload.birth_date, 
      state.user.name = payload.name, 
      state.user.surname = payload.surname,
      state.user.email = payload.email,
      state.user.email_parent = payload.email_parent,
      state.user.phone_parent = payload.phone_parent,
      state.user.login = payload.login,
      state.user.password = payload.password,
      state.user.role = payload.role, 
      state.user.accessToken = payload.accessToken,
      state.user.refreshToken = payload.refreshToken
      state.user.accessTokenExpiration = loginHelper.getTokenExpiration(payload.accessToken)
    },
    setUserAsTeacher(state, payload){
      state.user.name = payload.name, 
      state.user.surname = payload.surname,
      state.user.title = payload.title,
      state.user.email = payload.email,
      state.user.login = payload.login,
      state.user.password = payload.password,
      state.user.role = payload.role, 
      state.user.accessToken = payload.accessToken,
      state.user.refreshToken = payload.refreshToken
      state.user.accessTokenExpiration = loginHelper.getTokenExpiration(payload.accessToken)
    },
    setInitialUser(state){
      state.user.name = "",
      state.user.surname = "",
      state.user.title = "",
      state.user.email = "",
      state.user.login = "",
      state.user.password = "",
      state.user.role = "",
      state.user.class_id = null,
      state.user.birth_date = "",
      state.user.email_parent = "",
      state.user.phone_parent = "",
      state.user.accessToken = "",
      state.user.refreshToken = "",
      state.user.accessTokenExpiration = null
    },
    setNewUserToken(state, payload){
      state.user.accessToken = payload
      state.user.accessTokenExpiration = loginHelper.getTokenExpiration(payload)
    },
    addTaskSolutionFile(state, payload){
      state.task.taskSolution.files.push(payload)
    },
    addAssignmentFile(state, payload){
      state.task.assignment.files.push(payload)
    },
    resetAssignmentFiles(state){
      state.task.assignment.files = []
    }
  }, //synchronous
  actions: {}, //asynchronous
  modules: {},
  getters:{
    getAppState(state){
      return state.appState;
    },
    getUser(state){
      return state.user;
    },
    getTaskSolutionFiles(state){
      return state.task.taskSolution.files
    },
    getAssignmentFiles(state){
      return state.task.assignment.files
    },
    getActiveTaskId(state) {
      return state.task.activeTaskId;
    }
  }
});
