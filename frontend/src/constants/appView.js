import tasksIcon from "../assets/tasks_icon.svg";

export const VIEW_FILE = "VIEW_FILE"
export const VIEW_ADD_NEW_FILE = "VIEW_ADD_NEW_FILE"
export const LOGIN = "LOGIN"
export const SHOW_TASK = "SHOW_TASK"
export const SHOW_SOLUTIONS = "SHOW_SOLUTIONS"
export const ADD_TASK = "ADD_TASK"
export const TASK_EVALUATION = "TASK_EVALUATION"
export const SHOW_ASSIGNMENTS = "SHOW_ASSIGNMENTS"
export const EDIT_TASK = "EDIT_TASK"
export const MY_TASKS = "MY_TASKS"

export const views = [
    "VIEW_FILE",
    "VIEW_ADD_NEW_FILE",
    "SHOW_TASK",
    "SHOW_SOLUTIONS",
    "TASK_EVALUATION",
    "ADD_TASK",
    "SHOW_ASSIGNMENTS",
    "EDIT_TASK",
    "MY_TASKS"
]

export const viewsMenu = [
    ["SHOW_ASSIGNMENTS","Zadání", tasksIcon],
    ["MY_TASKS", "Moje úkoly", tasksIcon]
]
