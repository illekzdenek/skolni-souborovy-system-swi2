//aplikační server
export const GET_FILES = "http://127.0.0.1:30003/file/async"
export const ADD_FILE = "http://127.0.0.1:30003/file/async"
export const CONNECTION_TEST_MAIN = "http://127.0.0.1:30003/files/test"
export const GET_ALL_STUDENTS = "http://127.0.0.1:30003/data/students"
export const CREATE_TASK_SOLUTION = "http://127.0.0.1:30003/files/create_task_solution"
export const GET_TASK = "http://127.0.0.1:30003/data/task/" //lomítko na konci je tu důležité protože se za něj přidává id 
export const DELETE_TASK = "http://127.0.0.1:30003/data/delete_task/" //lomítko na konci je tu důležité protože se za něj přidává id 
export const GET_SUBJECT = "http://127.0.0.1:30003/data/subject/"
export const GET_SUBJECT_TASKS = "http://127.0.0.1:30003/data/subject_tasks/" 
export const GET_ASSIGNMENT = "http://127.0.0.1:30003/data/assignment/"
export const GET_ALL_ASSIGNMENTS = "http://127.0.0.1:30003/data/assignments"
export const DOWNLOAD_FILE = "http://127.0.0.1:30003/data/file_download/"
export const GET_TEACHERS_SUBJECTS = "http://127.0.0.1:30003/data/teachers_subjects/"
export const GET_TASK_SOLUTION = "http://127.0.0.1:30003/data/task_solution/"
export const GET_TASK_SOLUTIONS = "http://127.0.0.1:30003/data/task_solutions/" // for teacher for certain task all solutions
export const GET_STUDENT = "http://127.0.0.1:30003/data/student/"
export const GET_CLASS = "http://127.0.0.1:30003/data/class/"
export const ADD_SCORE = "http://127.0.0.1:30003/data/add_score"
export const CREATE_TASK = "http://127.0.0.1:30003/data/create_task"
export const CREATE_ASSIGNMENT = "http://127.0.0.1:30003/files/create_assignment"
export const EDIT_TASK = "http://127.0.0.1:30003/files/edit_task"
export const GET_CLASS_TO_SUBJECT = "http://127.0.0.1:30003/data/class_to_subjects/"

//autorizační server
export const LOGIN = "http://127.0.0.1:40004/server/login"
export const NEW_ACCESS_TOKEN = "http://127.0.0.1:40004/server/token"
export const CONNECTION_TEST_AUTH = "http://127.0.0.1:40004/server/test"
export const LOGOUT = "http://127.0.0.1:40004/server/logout"