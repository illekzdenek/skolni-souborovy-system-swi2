export async function fileToBase64(file) {
    function getBase64(file) {
        const reader = new FileReader();
        return new Promise(resolve => {
        reader.onload = ev => {
            resolve(ev.target.result);
        };
        reader.readAsDataURL(file);
        });
    }

    return await getBase64(file);
}

export function dataToFile(data, mime){
    const blob = new Blob([data], {type: mime || "application/octet-stream"})
    const blobUrl = (window.URL && window.URL.createObjectURL) ? window.URL.createObjectURL(blob) : window.webkitURL.createObjectURL(blob);
    return blobUrl
}