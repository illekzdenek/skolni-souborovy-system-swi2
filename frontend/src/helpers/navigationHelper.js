import * as appView from "../constants/appView"
import * as roles from "../constants/roles"

export function isAllowedToView(view, userRole){
    let possibleViews = []
    if (userRole === roles.ROLE_STUDENT){
        possibleViews=[
            appView.LOGIN,
            appView.MY_TASKS
        ]
        console.log(view)
        console.log(possibleViews)
        return possibleViews.includes(view)
    }
    if (userRole === roles.ROLE_TEACHER){
        possibleViews=[
            appView.LOGIN,
            appView.SHOW_TASK,
            appView.TASK_EVALUATION,
            appView.EDIT_TASK,
            appView.SHOW_ASSIGNMENTS,
            appView.SHOW_SOLUTIONS
        ]
        return possibleViews.includes(view)
    }
}

export function isAllowedToAction(action, user){
    console.log(action, user)
    //TODO: funkce pro udělování přístupu jednotlivým userům (aby si žáci nemohli měnit data navzájem)
}