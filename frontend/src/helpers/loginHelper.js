
export function decodeToken(token){
    console.log(token)
    return JSON.parse(atob(token.split('.')[1]))
}

export function getTokenExpiration(token){
    token = decodeToken(token)
    const tokenExpiration = token.exp
    const tokenCreated = token.iat
    const time = new Date(Date.now() + (tokenExpiration-tokenCreated)*1000)
    return time
}