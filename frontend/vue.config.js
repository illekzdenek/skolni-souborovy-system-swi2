const path = require("path")
module.exports = {
  publicPath: "/",
  runtimeCompiler: true,
  lintOnSave: process.env.NODE_ENV !== "production"
};
